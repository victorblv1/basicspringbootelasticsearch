package com.viktorsb.basicspringbootelasticsearch;

import com.viktorsb.basicspringbootelasticsearch.model.Employee;
import com.viktorsb.basicspringbootelasticsearch.repository.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;
import java.util.List;

@SpringBootApplication
public class BasicspringbootelasticsearchApplication implements CommandLineRunner {

	@Resource
	EmployeeRepository employeeRepository;

	public static void main(String[] args) {
		SpringApplication.run(BasicspringbootelasticsearchApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		Employee emp1 = new Employee("1", "Jared", "Vennett", 30);
		Employee emp2 = new Employee("2", "Sylvester", "Gabe", 28);
		Employee emp3 = new Employee("3", "Jörg", "Jänssen", 31);
		Employee emp4 = new Employee("4", "Samy", "Sonnenshine", 25);
		Employee emp5 = new Employee("5", "May", "Go", 22);
		Employee emp6 = new Employee("6", "Annette", "Geisberg", 28);

		// saving employees into the Elasticsearch
		System.out.println("==================Save Employees==================");
		employeeRepository.save(emp1);
		employeeRepository.save(emp2);
		employeeRepository.save(emp3);
		employeeRepository.save(emp4);
		employeeRepository.save(emp5);
		employeeRepository.save(emp6);

		/** method findByOne no longer working in Spring 2.20 release*/
		// find an Employee by Id
//		System.out.println("============Find an employee by id = 5============");
//		Employee empid3 = employeeRepository.findOne();
//		System.out.println(empid3);

		// finding all
		System.out.println("=====================Find All=====================");
		Iterable<Employee> employees = employeeRepository.findAll();
		employees.forEach(System.out::println);

		// find an Employee by first name 'Jörg'
		System.out.println("=======Find an employee by first name 'Jörg'======");
		List<Employee> empJoerg = employeeRepository.findByFirstName("Jörg");
		empJoerg.forEach(System.out::println);
	}
}
