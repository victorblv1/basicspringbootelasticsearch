package com.viktorsb.basicspringbootelasticsearch.repository;

import com.viktorsb.basicspringbootelasticsearch.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface EmployeeRepository extends ElasticsearchRepository<Employee,String> {

  List<Employee> findByFirstName(String firstName);
  Page<Employee> findByFirstName(String firstName, Pageable pageable);
  List<Employee> findByAge(int age);
}
