package com.viktorsb.basicspringbootelasticsearch.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "basicspringbootelasticsearch", type = "employee")
public class Employee {

  @Id
  private String id;

  private String firstName;
  private String lastName;
  private int age;

  public Employee() {}

  public Employee(String id, String firstName, String lastName, int age) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String toString() {
    String info = String.format(
            "Customer Info id = %s,\nfirst name = %s,\nlastname = %s,\nage = %d",
            id, firstName, lastName, age);
    return info;
  }
}
